//
//  DataCollectionViewCell.swift
//  LazyZalandoApp
//
//  Created by iosdev on 23.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class DataCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    
}
